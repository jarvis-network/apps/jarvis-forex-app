import React, { useEffect, useState } from "react";

import { styled } from "@mui/material";

import tokens from "config/tokens";

import { Wrapper } from "../styles";
import ProtocolFees from "./ProtocolFees";
import Route from "./Route";
import buildSwapRoute from "utils/buildSwapRoute";
import { getSinglePairOutput } from "utils/getSinglePairOutput";
import { useUsdPrices } from "state/trading/hooks";
import { useNetworkChainId } from "state/network/hooks";

interface SwapInfoProps {
  tokenIn: string;
  tokenOut: string;
  tokenInAmount: any;
  route: string[];
}

export const Container = styled(Wrapper)({
  flexDirection: "column",
  marginTop: "24px",
  padding: "16px",
  svg: {
    width: "12px",
    height: "12px",
    margin: "0px 2px 0px 2px",
  },
});

const SwapInfo: React.FC<SwapInfoProps> = ({
  tokenIn,
  tokenOut,
  tokenInAmount,
  route
}) => {
  const prices = useUsdPrices();
  const chainId = useNetworkChainId();

  useEffect(() => {
    const getAmountOut = async () => {
      let lastAmountOut = 1000000;
      for (let i = 0; i < route.length - 1; i++) {
        const tokenInObject = tokens.find((t) => t.symbol === route[i]);
        const tokenOutObject = tokens.find((t) => t.symbol === route[i + 1]);
        const output = await getSinglePairOutput(
          chainId,
          lastAmountOut,
          tokenInObject,
          tokenOutObject,
          prices
        );
        lastAmountOut = Number(output);
      }
    };

    getAmountOut();
  }, [tokenIn, tokenOut, route]);

  return (
    <Container>
      <ProtocolFees
        route={route}
        tokenIn={tokenIn}
        tokenOut={tokenOut}
        tokenInAmount={tokenInAmount}
      />
      <Route route={route} />
    </Container>
  );
};

export default SwapInfo;
