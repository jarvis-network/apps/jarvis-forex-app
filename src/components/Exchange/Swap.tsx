import React, { useEffect, useState } from "react";

import { formatUnits } from "ethers/lib/utils";

import { useBalanceFromSymbol } from "state/balance/hooks";
import { usePairsAndRoutesFromSymbol, useUsdPrices } from "state/trading/hooks";
import { getAmountIn, getAmountOut } from "utils/getAmount";

import ActionsButton from "./components/ActionsButton";
import AmountField from "./components/AmountField";
import Balance from "./components/Balance";
import ReverseToken from "./components/ReverseToken";
import { ExchangeBody } from "./components/styles";
import TradingRate from "./components/TradingRate";
import tokens from "config/tokens";
import buildSwapRoute from "utils/buildSwapRoute";

interface Props {
  tokenIn: string;
  tokenOut: string;
  tokenInAmount: string;
  tokenOutAmount: string;
  route: string[];
  setTokenInAmount: (amount: string) => void;
  setTokenOutAmount: (amount: string) => void;
  setView: (view: string) => void;
  handleReverseToken: () => void;
}

const Swap: React.FC<Props> = ({
  tokenIn,
  tokenOut,
  tokenInAmount,
  tokenOutAmount,
  route,
  setTokenInAmount,
  setTokenOutAmount,
  handleReverseToken,
  setView,
}) => {
  const [loading, setLoading] = useState(false);
  const [warning, setWarning] = useState(false);
  const prices = useUsdPrices();
  const tokenInBalance = useBalanceFromSymbol(tokenIn);
  const tokenOutBalance = useBalanceFromSymbol(tokenOut);
  const tradingPair = usePairsAndRoutesFromSymbol(tokenIn, tokenOut);

  const handleSelectMax = () => {
    setTokenInAmount(
      formatUnits(tokenInBalance.userBalance, tokenInBalance.decimals)
    );

    getAmountOut(
      Number(formatUnits(tokenInBalance.userBalance, tokenInBalance.decimals)),
      route,
      prices
    ).then((res) => setTokenOutAmount(Number(res).toFixed(6).toString()));
  };

  useEffect(() => {
    const updateAmountOut = async () => {
      const newAmountOut = await getAmountOut(
        Number(tokenInAmount),
        route,
        prices
      );
      setTokenOutAmount(Number(newAmountOut).toFixed(6).toString());
    };

    const delayDebounceFn = setTimeout(async () => {
      if (tokenInAmount && tokenInAmount !== '') {
        updateAmountOut();
      }
      setLoading(false)
    }, 1000)

    setLoading(true)
    return () => clearTimeout(delayDebounceFn)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tokenInAmount, setTokenOutAmount, route]);

  useEffect(() => {
    if (tokenInBalance) {
      setWarning(
        Number(
          formatUnits(tokenInBalance.userBalance, tokenInBalance.decimals)
        ) < Number(tokenInAmount)
      );
    }
  }, [tokenInBalance, tokenInAmount]);

  return (
    <>
      <ExchangeBody sx={{ padding: "32px 16px 16px" }}>
        <Balance label="You swap" balance={tokenInBalance} warning={warning} />
        <AmountField
          warning={warning}
          token={tokenIn}
          tradingPair={tradingPair}
          amount={tokenInAmount}
          setAmount={setTokenInAmount}
          setRelevantAmount={setTokenOutAmount}
          setView={setView}
          toView={"CHOOSE_ASSET_0"}
          computeRelevantAmount={getAmountOut}
          selectMax={handleSelectMax}
        />
        <ReverseToken handleReverseToken={handleReverseToken} />
        <Balance label="For" balance={tokenOutBalance} />
        <AmountField
          warning={warning}
          token={tokenOut}
          tradingPair={tradingPair}
          amount={tokenOutAmount}
          setAmount={setTokenOutAmount}
          setRelevantAmount={setTokenInAmount}
          setView={setView}
          toView={"CHOOSE_ASSET_1"}
          computeRelevantAmount={getAmountIn}
          loading={loading}
        />
        <TradingRate tradingPair={tradingPair} />
        <ActionsButton
          tradingPair={tradingPair}
          tokenInBalance={tokenInBalance}
          tokenOutBalance={tokenOutBalance}
          tokenInAmount={tokenInAmount}
          setTokenInAmount={setTokenInAmount}
          setTokenOutAmount={setTokenOutAmount}
        />
      </ExchangeBody>
    </>
  );
};

export default Swap;
