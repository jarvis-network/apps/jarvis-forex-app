import tokens from "config/tokens";
import { getSinglePairOutput } from "./getSinglePairOutput";

export const getAmountIn = async (
  amountOut: number,
  route: string[],
  prices: any
) => {
  if (route && amountOut && prices) {
    const amountOutFromArg = await getAmountOut(amountOut, route, prices)
    const ratio = amountOutFromArg !== "" ? amountOut / Number(amountOutFromArg) : 0
    return ratio !== 0 ? (amountOut * ratio).toString() : ""
  }
  return "";
};

export const getAmountOut = async (
  amountIn: number,
  route: string[],
  prices: any
) => {
  if (route && amountIn && prices) {
    let lastAmountOut = amountIn;
    for (let i = 0; i < route.length - 1; i++) {
      const tokenInObject = tokens.find((t) => t.symbol === route[i]);
      const tokenOutObject = tokens.find((t) => t.symbol === route[i + 1]);
      const output = await getSinglePairOutput(
        137,
        lastAmountOut,
        tokenInObject,
        tokenOutObject,
        prices
      );
      lastAmountOut = Number(output);
    }
    return lastAmountOut.toString()
  }
  return "";
};
