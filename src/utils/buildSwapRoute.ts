import tokens from "config/tokens";
import { Token, TokenTypes } from "state/types";

const buildSwapRoute = (tokenIn: Token, tokenOut: Token) => {
  const collateral = tokens.find((t) => t.type.includes(TokenTypes.collateral));
  const fromTypes = tokenIn.type;
  const toTypes = tokenOut.type;

  let route = [tokenIn.symbol, tokenOut.symbol];
  // jSynth
  if (fromTypes.includes(TokenTypes.jSynth)) {
    if (toTypes.includes(TokenTypes.metapool)) {
      route = [tokenIn.symbol, collateral.symbol, tokenOut.symbol];
    } else if (!toTypes.includes(TokenTypes.jSynth)) {
      if (tokenOut.jSynthAssociated === tokenIn.symbol) {
        route = [tokenIn.symbol, tokenOut.symbol];
      }
      else {
        route = [tokenIn.symbol, tokenOut.jSynthAssociated, tokenOut.symbol];
      }
    }
  }
  // Collateral
  else if (fromTypes.includes(TokenTypes.collateral)) {
    if (toTypes.includes(TokenTypes.metapool)) {
      route = [tokenIn.symbol, collateral.symbol, tokenOut.symbol];
    } else if (!toTypes.includes(TokenTypes.jSynth)) {
      route = [
        tokenIn.symbol,
        collateral.symbol,
        tokenOut.jSynthAssociated,
        tokenOut.symbol,
      ];
    }
  } else if (fromTypes.includes(TokenTypes.metapool)) {
    if (toTypes.includes(TokenTypes.jSynth)) {
      route = [tokenIn.symbol, collateral.symbol, tokenOut.symbol];
    } else if (!toTypes.includes(TokenTypes.metapool)) {
      route = [
        tokenIn.symbol,
        collateral.symbol,
        tokenOut.jSynthAssociated,
        tokenOut.symbol,
      ];
    }
  } else if (fromTypes.includes(TokenTypes.stablecoin)) {
    if (toTypes.includes(TokenTypes.jSynth)) {
      route = [tokenIn.symbol, tokenIn.jSynthAssociated, tokenOut.symbol];
    } else if (toTypes.includes(TokenTypes.stablecoin)) {
      if (tokenIn.curvePool === tokenOut.curvePool) {
        route = [tokenIn.symbol, tokenOut.symbol];
      } else if (toTypes.includes(TokenTypes.metapool)) {
        route = [
          tokenIn.symbol,
          tokenIn.jSynthAssociated,
          collateral.symbol,
          tokenOut.symbol,
        ];
      } else {
        route = [
          tokenIn.symbol,
          tokenIn.jSynthAssociated,
          tokenOut.jSynthAssociated,
          tokenOut.symbol,
        ];
      }
    }
  } else {
    if (toTypes.includes(TokenTypes.metapool)) {
      if (fromTypes.includes(TokenTypes.stablecoin)) {
        route = [tokenIn.symbol, tokenOut.jSynthAssociated, tokenOut.symbol];
      } else {
        route = [
          tokenIn.symbol,
          tokenIn.jSynthAssociated,
          collateral.symbol,
          tokenOut.symbol,
        ];
      }
    } else if (toTypes.includes(TokenTypes.collateral)) {
      route = [tokenIn.symbol, tokenIn.jSynthAssociated, tokenOut.symbol];
    }
  }

  route = [...new Set(route)];
  return route;
};

export default buildSwapRoute;
