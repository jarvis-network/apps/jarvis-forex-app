import { Token, TokenTypes } from "state/types";
import curvePoolAbi from "config/abi/curvePool.json";
import multicall from "utils/multicall";
import { formatUnits, parseUnits } from "ethers/lib/utils";
import { BigNumber } from "ethers";

export const getSinglePairOutput = async (
  chainId: number,
  amountIn: number,
  tokenInObject: Token,
  tokenOutObject: Token,
  prices: any
) => {
  const findPrice = (symbol: string) => {
    return prices.find((p) => p.symbol === symbol);
  };

  if (
    tokenInObject.type.includes(TokenTypes.metapool) &&
    tokenOutObject.type.includes(TokenTypes.metapool)
  ) {
    const rawExchangeRate = await multicall(chainId, curvePoolAbi, [
      {
        address: tokenOutObject.curvePool,
        name: "get_dy_underlying",
        params: [
          tokenInObject.tokenIndexInCurvePool,
          tokenOutObject.tokenIndexInCurvePool,
          BigNumber.from(amountIn.toFixed(0)).mul(BigNumber.from(10).pow(tokenInObject.decimals))
        ],
      },
    ]);
    return formatUnits(rawExchangeRate[0][0], tokenOutObject.decimals);
  } else if (
    (tokenInObject.type.includes(TokenTypes.jSynth) &&
      tokenOutObject.type.includes(TokenTypes.stablecoin) &&
      !tokenOutObject.type.includes(TokenTypes.metapool)) ||
    (tokenInObject.type.includes(TokenTypes.stablecoin) &&
      !tokenInObject.type.includes(TokenTypes.metapool) &&
      tokenOutObject.type.includes(TokenTypes.stablecoin) &&
      !tokenOutObject.type.includes(TokenTypes.metapool)) ||
    (tokenInObject.type.includes(TokenTypes.stablecoin) &&
      !tokenInObject.type.includes(TokenTypes.metapool) &&
      tokenOutObject.type.includes(TokenTypes.jSynth))
  ) {
    let specificCurvePool;
    let tokenInIndex;
    let tokenOutIndex;
    if(tokenInObject.reversedIndexOrder) {
      specificCurvePool = tokenInObject.curvePool;
      tokenInIndex = tokenInObject.tokenIndexInCurvePool;
      tokenOutIndex = tokenInObject.associatedTokenIndexInCurvePool;
    } else if(tokenOutObject.reversedIndexOrder) {
      specificCurvePool = tokenOutObject.curvePool;
      tokenInIndex = tokenOutObject.associatedTokenIndexInCurvePool;
      tokenOutIndex = tokenOutObject.tokenIndexInCurvePool;
    } else {
      specificCurvePool = tokenOutObject.curvePool;
      tokenInIndex = tokenInObject.tokenIndexInCurvePool;
      tokenOutIndex = tokenOutObject.tokenIndexInCurvePool;
    }
    const rawExchangeRate = await multicall(chainId, curvePoolAbi, 
      [
      {
        address: specificCurvePool,
        name: "get_dy",
        params: [
          tokenInIndex,
          tokenOutIndex,
          BigNumber.from(amountIn.toFixed(0)).mul(BigNumber.from(10).pow(tokenInObject.decimals))
        ],
      },
    ]);
    return formatUnits(rawExchangeRate[0][0], tokenOutObject.decimals);
  } else if (
    tokenInObject.type.includes(TokenTypes.jSynth) &&
    tokenOutObject.type.includes(TokenTypes.jSynth)
  ) {
    return (
      amountIn *
      (findPrice(tokenInObject.symbol).usdPrice /
        findPrice(tokenOutObject.symbol).usdPrice)
    );
  } else if (
    tokenInObject.type.includes(TokenTypes.jSynth) &&
    tokenOutObject.type.includes(TokenTypes.collateral)
  ) {
    return amountIn * findPrice(tokenInObject.symbol).usdPrice;
  } else if (
    tokenInObject.type.includes(TokenTypes.collateral) &&
    tokenOutObject.type.includes(TokenTypes.jSynth)
  ) {
    return amountIn * (1 / findPrice(tokenOutObject.symbol).usdPrice);
  }
};
