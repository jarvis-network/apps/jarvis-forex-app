const CURVE_IDS = [
  "factory-v2-22",
  "factory-v2-23",
  "factory-v2-37",
  "factory-v2-85",
  "factory-v2-107",
  "factory-v2-209",
  "factory-v2-228",
  "factory-v2-286",
  "factory-v2-285",
];

export default CURVE_IDS;
