import { Token, TokenTypes } from "state/types";

const tokens: Token[] = [
  // SynthetiX
  {
    symbol: "jEUR",
    address: "0x4e3decbb3645551b8a19f0ea1678079fcb33fb4c",
    type: [TokenTypes.jSynth],
    decimals: 18,
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0xAd326c253A84e9805559b73A08724e11E49ca651",
    tokenIndexInCurvePool: 0,
    chainlinkFeed: "0x73366Fe0AA0Ded304479862808e02506FE556a98",
    reversedIndexOrder: false
  },
  {
    symbol: "jSGD",
    address: "0xa926db7a4CC0cb1736D5ac60495ca8Eb7214B503",
    type: [TokenTypes.jSynth],
    decimals: 18,
    pool: "0xBE813590e1B191120f5df3343368f8a2F579514C",
    curvePool: "0xeF75E9C7097842AcC5D0869E1dB4e5fDdf4BFDDA",
    tokenIndexInCurvePool: 0,
    chainlinkFeed: "0x8CE3cAc0E6635ce04783709ca3CC4F5fc5304299",
    reversedIndexOrder: false
  },
  {
    symbol: "jCAD",
    address: "0x8ca194A3b22077359b5732DE53373D4afC11DeE3",
    type: [TokenTypes.jSynth],
    decimals: 18,
    pool: "0x06440a2DA257233790B5355322dAD82C10F0389A",
    curvePool: "0xA69b0D5c0C401BBA2d5162138613B5E38584F63F",
    tokenIndexInCurvePool: 0,
    chainlinkFeed: "0xACA44ABb8B04D07D883202F99FA5E3c53ed57Fb5",
    reversedIndexOrder: false
  },
  {
    symbol: "jJPY",
    address: "0x8343091F2499FD4b6174A46D067A920a3b851FF9",
    type: [TokenTypes.jSynth],
    decimals: 18,
    pool: "0xAEc757BF73cc1f4609a1459205835Dd40b4e3F29",
    curvePool: "0xE8dCeA7Fb2Baf7a9F4d9af608F06d78a687F8d9A",
    tokenIndexInCurvePool: 0,
    chainlinkFeed: "0xD647a6fC9BC6402301583C91decC5989d8Bc382D",
    reversedIndexOrder: false
  },
  {
    symbol: "jNZD",
    address: "0x6b526Daf03B4C47AF2bcc5860B12151823Ff70E0",
    type: [TokenTypes.jSynth],
    decimals: 18,
    pool: "0x4FDA1B4b16f5F2535482b91314018aE5A2fda602",
    curvePool: "0x976A750168801F58E8AEdbCfF9328138D544cc09",
    tokenIndexInCurvePool: 0,
    chainlinkFeed: "0xa302a0B8a499fD0f00449df0a490DedE21105955",
    reversedIndexOrder: false,
  },

  // Stablecoins

  {
    symbol: "agEUR",
    address: "0xE0B52e49357Fd4DAf2c15e02058DCE6BC0057db4",
    decimals: 18,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x2fFbCE9099cBed86984286A54e5932414aF4B717",
    tokenIndexInCurvePool: 1,
    reversedIndexOrder: false
  },
  {
    symbol: "EURS",
    address: "0xE111178A87A3BFf0c8d18DECBa5798827539Ae99",
    decimals: 2,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0xAd326c253A84e9805559b73A08724e11E49ca651",
    tokenIndexInCurvePool: 2,
    reversedIndexOrder: false
  },
  {
    symbol: "CADC",
    address: "0x5d146d8B1dACb1EBBA5cb005ae1059DA8a1FbF57",
    decimals: 18,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jCAD",
    pool: "0x06440a2DA257233790B5355322dAD82C10F0389A",
    curvePool: "0xA69b0D5c0C401BBA2d5162138613B5E38584F63F",
    tokenIndexInCurvePool: 1,
    reversedIndexOrder: false
  },
  {
    symbol: "PAR",
    address: "0xE2Aa7db6dA1dAE97C5f5C6914d285fBfCC32A128",
    decimals: 18,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x0f110c55EfE62c16D553A3d3464B77e1853d0e97",
    tokenIndexInCurvePool: 0,
    associatedTokenIndexInCurvePool: 1,
    reversedIndexOrder: true,
  },
  {
    symbol: "EURT",
    address: "0x7BDF330f423Ea880FF95fC41A280fD5eCFD3D09f",
    decimals: 6,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x2C3cc8e698890271c8141be9F6fD6243d56B39f1",
    tokenIndexInCurvePool: 0,
    associatedTokenIndexInCurvePool: 1,
    reversedIndexOrder: true,
  },
  {
    symbol: "JPYC",
    address: "0x6AE7Dfc73E0dDE2aa99ac063DcF7e8A63265108c",
    decimals: 18,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jJPY",
    pool: "0xAEc757BF73cc1f4609a1459205835Dd40b4e3F29",
    curvePool: "0xE8dCeA7Fb2Baf7a9F4d9af608F06d78a687F8d9A",
    tokenIndexInCurvePool: 1,
    reversedIndexOrder: false
  },
  {
    symbol: "XSGD",
    address: "0x769434dcA303597C8fc4997Bf3DAB233e961Eda2",
    decimals: 6,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jSGD",
    pool: "0xBE813590e1B191120f5df3343368f8a2F579514C",
    curvePool: "0xeF75E9C7097842AcC5D0869E1dB4e5fDdf4BFDDA",
    tokenIndexInCurvePool: 1,
    reversedIndexOrder: false
  },
  {
    symbol: "NZDS",
    address: "0xeaFE31Cd9e8E01C8f0073A2C974f728Fb80e9DcE",
    decimals: 6,
    type: [TokenTypes.stablecoin],
    jSynthAssociated: "jNZD",
    pool: "0x4FDA1B4b16f5F2535482b91314018aE5A2fda602",
    curvePool: "0x976A750168801F58E8AEdbCfF9328138D544cc09",
    tokenIndexInCurvePool: 1,
    reversedIndexOrder: false
  },

  {
    symbol: "USDC",
    address: "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174",
    decimals: 6,
    type: [TokenTypes.stablecoin, TokenTypes.collateral, TokenTypes.metapool],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x447646e84498552e62eCF097Cc305eaBFFF09308",
    tokenIndexInCurvePool: 2,
    chainlinkFeed: "0xfE4A8cc5b5B2366C1B58Bea3858e81843581b2F7",
    reversedIndexOrder: false
  },

  {
    symbol: "MAI",
    address: "0xa3Fa99A148fA48D14Ed51d610c367C61876997F1",
    decimals: 18,
    type: [TokenTypes.stablecoin, TokenTypes.metapool],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x447646e84498552e62eCF097Cc305eaBFFF09308",
    tokenIndexInCurvePool: 0,
    reversedIndexOrder: false
  },

  {
    symbol: "DAI",
    address: "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063",
    decimals: 18,
    type: [TokenTypes.stablecoin, TokenTypes.metapool],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x447646e84498552e62eCF097Cc305eaBFFF09308",
    tokenIndexInCurvePool: 1,
    chainlinkFeed: "0x4746DeC9e833A82EC7C2C1356372CcF2cfcD2F3D",
    reversedIndexOrder: false
  },
  {
    symbol: "USDT",
    address: "0xc2132D05D31c914a87C6611C10748AEb04B58e8F",
    decimals: 6,
    type: [TokenTypes.stablecoin, TokenTypes.metapool],
    jSynthAssociated: "jEUR",
    pool: "0x65a7b4Ff684C2d08c115D55a4B089bf4E92F5003",
    curvePool: "0x447646e84498552e62eCF097Cc305eaBFFF09308",
    tokenIndexInCurvePool: 3,
    chainlinkFeed: "0x0A6513e40db6EB1b165753AD52E80663aeA50545",
    reversedIndexOrder: false
  },
];

export default tokens;
