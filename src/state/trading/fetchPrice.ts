import { formatUnits } from "ethers/lib/utils";

import chainlinkAbi from "config/abi/chainlink.json";
import CURVE_IDS from "config/pools";
import tokens from "config/tokens";
import multicall from "utils/multicall";



function calculateUsdPrice(referenceToken, desiredToken, tokensPriceInUsd) {
  if (referenceToken.poolBalance === "0" || desiredToken === "0"){
    return 0;
  }
  const scaledReferenceToken = Number(referenceToken.poolBalance / (10**referenceToken.decimals));
  const scaledDesiredToken = Number(desiredToken.poolBalance / (10** desiredToken.decimals));
  const curvePoolPrice = scaledDesiredToken / scaledReferenceToken
  let referenceTokenUsdPrice = 0;
  for (let i = 0; i < tokensPriceInUsd.length; i ++) {
    if (tokensPriceInUsd[i].symbol === 'jNZD'){
      referenceTokenUsdPrice = tokensPriceInUsd[i].usdPrice
    }
    else if (tokensPriceInUsd[i].symbol === 'jNGN'){
      referenceTokenUsdPrice = tokensPriceInUsd[i].usdPrice
    }
  }
  const finalPriceInUsd = curvePoolPrice * referenceTokenUsdPrice;
  return finalPriceInUsd
}

// const CURVE_API_URL = "https://curve-api-git-main-curvefi.vercel.app/api/getPools/polygon/factory"
const CURVE_API_URL =
  "https://curve-api-git-main-curvefi.vercel.app/api/getFactoryV2Pools-polygon";

const CURVE_MAIN_API = "https://api.curve.fi/api/getPools/polygon/main";

const CURVE_MAIN_IDS = ["0"];


export const fetchPrice = async (chainId: number) => {
  const tokensPriceInUsd = [];

  const callsChainlinkPrice = [];
  const callsChainlinkDecimals = [];

  tokens.forEach((t) => {
    if (t.chainlinkFeed) {
      callsChainlinkPrice.push({
        address: t.chainlinkFeed,
        name: "latestAnswer",
      });
      callsChainlinkDecimals.push({
        address: t.chainlinkFeed,
        name: "decimals",
      });
    }
  });
  const rawPrices = await multicall(chainId, chainlinkAbi, callsChainlinkPrice);
  const rawDecimals = await multicall(
    chainId,
    chainlinkAbi,
    callsChainlinkDecimals
  );

  const chainlinkDecimals = rawDecimals.map((res) => res[0]);
  const chainlinkPrices = rawPrices.map((res, index) =>
    formatUnits(res[0], chainlinkDecimals[index])
  );

  let lastIndexUsed = 0;

  tokens.forEach((t) => {
    if (t.chainlinkFeed) {
      tokensPriceInUsd.push({
        symbol: t.symbol,
        address: t.address,
        usdPrice: Number(chainlinkPrices[lastIndexUsed]),
      });
      lastIndexUsed++;
    }
  });
  
  const response = await fetch(CURVE_API_URL);
  const responseMain = await fetch(CURVE_MAIN_API);

  const responseData = await response.json();
  const responseMainData = await responseMain.json();

  if (response.status === 200 || response.status === 201) {
    const poolData = responseData.data.poolData
    const filteredPoolData = poolData.filter((p) => CURVE_IDS.includes(p.id));
    filteredPoolData.forEach((pool) => {
      pool.coins.forEach((token) => {
        if (!tokensPriceInUsd.map((t) => t.symbol).includes(token.symbol)) {
          tokensPriceInUsd.push({
            symbol: token.symbol === "miMATIC" ? "MAI" : token.symbol,
            address: token.address,
            usdPrice: token.usdPrice ? token.usdPrice : calculateUsdPrice(pool.coins[0], pool.coins[1], tokensPriceInUsd),
          });
        }
      });
    });
  }

  if (responseMain.status === 200 || responseMain.status === 201) {
    const poolData = responseMainData.data.poolData;
    const filteredPoolData = poolData.filter((p) =>
      CURVE_MAIN_IDS.includes(p.id)
    );
    filteredPoolData.forEach((pool) => {
      pool.coins.forEach((token) => {
        if (!tokensPriceInUsd.map((t) => t.symbol).includes(token.symbol)) {
          tokensPriceInUsd.push({
            symbol: token.symbol.substring(2),
            address: token.address,
            usdPrice: token.usdPrice,
          });
        }
      });
    });
  }  
  return tokensPriceInUsd;
};
