import tokens from "config/tokens";
import { formatUnits, parseUnits } from "ethers/lib/utils";
import { TokenTypes } from "state/types";

import curvePoolAbi from "config/abi/curvePool.json";
import buildSwapRoute from "utils/buildSwapRoute";
import multicall from "utils/multicall";
import { TradingPair } from "./types";

export const fetchPairsPrice = async (
  chainId: number,
  pairsList: TradingPair[],
  prices: any
) => {
  const findPrice = (symbol: string) => {
    return prices.find((p) => p.symbol === symbol);
  };
  const singleHopPairs = [];
  const singleHopPairsExchangeRateCalls = [];

  pairsList.forEach((pair) => {
    const route = buildSwapRoute(pair.token0, pair.token1);
    for (let i = 0; i < route.length - 1; i++) {
      const tokenInObject = tokens.find((t) => t.symbol === route[i]);
      const tokenOutObject = tokens.find((t) => t.symbol === route[i + 1]);
      if (
        singleHopPairs.filter(
          (p) => p.token0 === tokenInObject && p.token1 === tokenOutObject
        ).length === 0
      ) {
        singleHopPairs.push({
          token0: tokenInObject,
          token1: tokenOutObject,
        });
      }
    }
  });

  singleHopPairs.forEach((pair) => {
    const tokenInObject = pair.token0;
    const tokenOutObject = pair.token1;

    if (
      tokenInObject.type.includes(TokenTypes.metapool) &&
      tokenOutObject.type.includes(TokenTypes.metapool)
    ) {
      singleHopPairsExchangeRateCalls.push({
        address: tokenOutObject.curvePool,
        name: "get_dy_underlying",
        params: [
          tokenInObject.tokenIndexInCurvePool,
          tokenOutObject.tokenIndexInCurvePool,
          (1 * 10 ** tokenInObject.decimals).toString(),
        ],
      });
    } else if (
      (tokenInObject.type.includes(TokenTypes.jSynth) &&
        tokenOutObject.type.includes(TokenTypes.stablecoin) &&
        !tokenOutObject.type.includes(TokenTypes.metapool)) ||
      (tokenInObject.type.includes(TokenTypes.stablecoin) &&
        !tokenInObject.type.includes(TokenTypes.metapool) &&
        tokenOutObject.type.includes(TokenTypes.stablecoin) &&
        !tokenOutObject.type.includes(TokenTypes.metapool)) ||
      (tokenInObject.type.includes(TokenTypes.stablecoin) &&
        !tokenInObject.type.includes(TokenTypes.metapool) &&
        tokenOutObject.type.includes(TokenTypes.jSynth))
    ) {
      if (tokenInObject.reversedIndexOrder) {
        singleHopPairsExchangeRateCalls.push({
          address: tokenInObject.curvePool,
          name: "get_dy",
          params: [
            tokenInObject.tokenIndexInCurvePool,
            tokenInObject.associatedTokenIndexInCurvePool,
            (1 * 10 ** tokenInObject.decimals).toString(),
          ],
        });
      } else if (tokenOutObject.reversedIndexOrder) {
        singleHopPairsExchangeRateCalls.push({
          address: tokenOutObject.curvePool,
          name: "get_dy",
          params: [
            tokenOutObject.associatedTokenIndexInCurvePool,
            tokenOutObject.tokenIndexInCurvePool,
            (1 * 10 ** tokenInObject.decimals).toString(),
          ],
        });
      } else {
        singleHopPairsExchangeRateCalls.push({
          address: tokenOutObject.curvePool,
          name: "get_dy",
          params: [
            tokenInObject.tokenIndexInCurvePool,
            tokenOutObject.tokenIndexInCurvePool,
            (1 * 10 ** tokenInObject.decimals).toString(),
          ],
        });
      }
    }
  });
  const rawExchangeRate = await multicall(
    chainId,
    curvePoolAbi,
    singleHopPairsExchangeRateCalls
  );
  
  let rawIndex = 0;
  singleHopPairs.forEach((pair) => {
    const tokenInObject = pair.token0;
    const tokenOutObject = pair.token1;

    if (
      (tokenInObject.type.includes(TokenTypes.metapool) &&
        tokenOutObject.type.includes(TokenTypes.metapool)) ||
      (tokenInObject.type.includes(TokenTypes.jSynth) &&
        tokenOutObject.type.includes(TokenTypes.stablecoin) &&
        !tokenOutObject.type.includes(TokenTypes.metapool)) ||
      (tokenInObject.type.includes(TokenTypes.stablecoin) &&
        !tokenInObject.type.includes(TokenTypes.metapool) &&
        tokenOutObject.type.includes(TokenTypes.stablecoin) &&
        !tokenOutObject.type.includes(TokenTypes.metapool)) ||
      (tokenInObject.type.includes(TokenTypes.stablecoin) &&
        !tokenInObject.type.includes(TokenTypes.metapool) &&
        tokenOutObject.type.includes(TokenTypes.jSynth))
    ) {
      pair.rate = Number(
        formatUnits(rawExchangeRate[rawIndex][0], tokenOutObject.decimals)
      );
      rawIndex++;
    } else if (
      tokenInObject.type.includes(TokenTypes.jSynth) &&
      tokenOutObject.type.includes(TokenTypes.jSynth)
    ) {
      pair.rate =
        findPrice(tokenInObject.symbol).usdPrice /
        findPrice(tokenOutObject.symbol).usdPrice;
    } else if (
      tokenInObject.type.includes(TokenTypes.jSynth) &&
      tokenOutObject.type.includes(TokenTypes.collateral)
    ) {
      pair.rate = findPrice(tokenInObject.symbol).usdPrice;
    } else if (
      tokenInObject.type.includes(TokenTypes.collateral) &&
      tokenOutObject.type.includes(TokenTypes.jSynth)
    ) {
      pair.rate = 1 / findPrice(tokenOutObject.symbol).usdPrice;
    }
  });

  const pairsWithRate = pairsList.map((pair) => {
    const route = buildSwapRoute(pair.token0, pair.token1);
    let lastRate = 0
    for (let i = 0; i < route.length - 1; i++) {
      const rate = singleHopPairs.find(p => p.token0.symbol === route[i] && p.token1.symbol === route[i + 1]).rate
      lastRate = lastRate === 0 ? 1 * rate : lastRate * rate
    }
    return {
      ...pair,
      rate: lastRate,
    };
  });

  return pairsWithRate;
};
